let DefaultColors = { fg: "#333", bg: "white" };
let currentColors = DefaultColors;

// usage example
let c = CColors()
  .Init({
    selector: "#ccolors-bar",
    prefix: "ccolors",
    defaultColors: DefaultColors
  })
  .UseA11y()
  .Draw();

// some "fun" font stuff
const MINFONT = 8;
const MAXFONT = 64;
const FONT_SIZE_MULT = 1.5;
const DEFAULT_FONT_SIZE = 16;
const RESET = "reset";
const LARGER = "+";
const SMALLER = "-";

function clickToChangeFontTo(fontFamily) {
  const body = document.querySelector("body");
  body.style.fontFamily = `${fontFamily}`;
}

function textBox(text) {
  const box = document.createElement("div");
  box.innerText = text;
  box.style.display = "inline-block";
  box.style.fontSize = "1.2rem";
  // box.style.border = "1px solid black";
  box.style.width = "1.2rem";
  box.style.height = "1.2rem";
  box.style.padding = "0.2rem";
  box.style.cursor = "pointer";
  box.className = "ccolors-fg ccolors-bg";
  return box;
}

function fontFamilyBox(fontFamily) {
  const box = textBox("I");
  box.style.fontFamily = fontFamily;
  box.onclick = () => clickToChangeFontTo(fontFamily);
  return box;
}

let currFontSize = 16;
function growOrShrinkOrReset(option) {
  let newSize;
  switch (option) {
    case LARGER:
      newSize = Math.floor(currFontSize * FONT_SIZE_MULT);
      break;
    case SMALLER:
      newSize = Math.floor(currFontSize / FONT_SIZE_MULT);
      break;
    default:
      newSize = 16;
  }
  if (MINFONT <= newSize && newSize <= MAXFONT) {
    const p = document.querySelector("p");
    p.style.fontSize = `${newSize}px`;
    currFontSize = newSize;
  }
}

const fontBar = document.getElementById("font-family-bar");
["monospace", "serif", "sans-serif"]
  .map(fontFamilyBox)
  .forEach(box => fontBar.appendChild(box));

const fontSizeBar = document.getElementById("font-size-bar");
[SMALLER, LARGER, RESET]
  .map(textBox)
  .map(box => {
    box.onclick = () => growOrShrinkOrReset(box.innerText);
    return box;
  })
  .forEach(box => fontSizeBar.appendChild(box));

const fgSelector = document.getElementById("fg-selector");
const bgSelector = document.getElementById("bg-selector");
fgSelector.onchange = newColor => {
  currentColors.fg = "#" + newColor.target.value;
  c.UpdateDefault(currentColors);
};
bgSelector.onchange = newColor => {
  currentColors.bg = "#" + newColor.target.value;
  c.UpdateDefault(currentColors);
};
