function CColors() {
  function _makeColorScheme(fg, bg) {
    return {
      fg: fg,
      bg: bg,
      barBorder: fg
    };
  }

  function styleColorBox(isFirstElement = false, border) {
    const kebab = word =>
      word
        .split("")
        .reduce(
          (a, c) => a + (c == c.toUpperCase() ? "-" : "") + c.toLowerCase(),
          ""
        );

    const styleObj = {
      display: "inline-block",
      width: "2rem",
      height: "2rem",
      cursor: "pointer"
    };

    // if (border) {
    //   styleObj.borderTop = "=px solid black";
    //   styleObj.borderBottom - "2px solid black";
    //   styleObj.borderRight = "2px solid black";
    //   styleObj.borderLeft = isFirstElement ? "2px solid black" : false;
    // }

    return Object.keys(styleObj).reduce((acc, currKey) => {
      const key = kebab(currKey);
      const val = styleObj[currKey];
      if (val) return `${acc}${key}: ${val}; `;
      return acc;
    }, "");
  }

  function makeColorBox(colors, id = -1, border = true) {
    const style = styleColorBox(id === -1, (border = true));
    const box = document.createElement("div");
    box.style = `${style}background-color: ${colors.bg}`;
    return onClickFn => {
      box.onclick = () => onClickFn(colors);
      return box;
    };
  }

  function ccolors({ selector, prefix, defaultColors, barBorder }) {
    let _defaultColors = defaultColors || _makeColorScheme("black", "white");
    let _selector = selector || "#ccolors-bar";
    let _prefix = prefix || "ccolors";
    let _colors = [];
    let border = barBorder || true;

    function changeColorOnClick(colors) {
      console.log(`${_prefix}-bg`);
      Array.from(document.querySelectorAll(`.${_prefix}-bg`)).forEach(
        k => (k.style.backgroundColor = colors.bg)
      );
      Array.from(document.querySelectorAll(`.${_prefix}-fg`)).forEach(
        k => (k.style.color = colors.fg)
      );
    }

    return {
      ColorBar: function(colors) {
        _colors = colors;
        return this;
      },
      UseA11y: function() {
        _colors = [
          {
            fg: "#7FDBFF",
            bg: "#333"
          },
          {
            fg: "#001F3F",
            bg: "#fff"
          },
          {
            fg: "#001F3F",
            bg: "#ddd"
          },
          {
            fg: "#111",
            bg: "#aaa"
          },
          {
            fg: "#111",
            bg: "#F012BE"
          },
          {
            fg: "#85144B",
            bg: "#FFDC00"
          },
          {
            fg: "#fff",
            bg: "#B10DC9"
          },
          {
            fg: "#001F3F",
            bg: "#FF851B"
          },
          {
            fg: "#FFDC00",
            bg: "#85144B"
          },
          {
            fg: "#001F3F",
            bg: "#FF4136"
          },
          {
            fg: "#001F3F",
            bg: "#7FDBFF"
          },
          {
            fg: "#fff",
            bg: "#0074D9"
          },
          {
            fg: "#01FF70",
            bg: "#001F3F"
          },
          {
            fg: "#001F3F",
            bg: "#39CCCC"
          },
          {
            fg: "#85144B",
            bg: "#01FF70"
          },
          {
            fg: "#fff",
            bg: "#3D9970"
          },
          {
            fg: "#111",
            bg: "#2ECC40"
          }
        ];
        return this;
      },
      Draw: function() {
        let colorBox = (c, i) => makeColorBox(c, i, border)(changeColorOnClick);

        let defaultColorBox = colorBox(_defaultColors, -1);
        defaultColorBox.id = "__ccolors-default-box";
        let colorBoxes = _colors.map((c, i) => colorBox(c, i));

        let colorBar = document.querySelector(_selector);
        colorBar.innerHTML = "";
        colorBar.appendChild(defaultColorBox);
        colorBoxes.forEach(box => colorBar.appendChild(box));
        return this;
      },
      UpdateDefault: function(colors) {
        _defaultColors = { fg: colors.fg, bg: colors.bg };
        this.Draw();
        // click the default
        const defaultBox = document.getElementById("__ccolors-default-box");
        defaultBox.click();

        return this;
      }
    };
  }

  return {
    Init: ccolors
  };
}
