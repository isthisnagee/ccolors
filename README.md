# ccolors
A little bit of color

https://isthisnagee.github.io/ccolors/

## usage

### HTML

In your HTML, add the following:

```
<div id="color-bar"></div>
```

The id used here will be what you put for the selector in the initialization.
In this case, the selector would be `#color-bar`. If you used a class here,
for example `class="color-bar"`, your selector would be `.color-bar`.

For any place where you want the background (bg) or foreground (fg) changed,
you need to specify so in the HTML. Choose a prefix, for example `ccolors`.
If I have the following block of text,
```
<p>
Hello world, please change my foreground color when clicking the color bar
</p>
```
I can change it's foreground by adding the folowwing.
```
<p class="ccolors-fg">
Hello world, please change my foreground color when clicking the color bar
</p>
```

If I want to also change the background color,
```
<p class="ccolors-fg ccolors-bg">

Hello world, please change my foreground color when clicking the color bar
```

### JS

In a JS file (or a `script` tag), add the following:

To use the default accessibility (a11y) colors:
```javascript
CColors()
  .Init({
    selector: "#color-bar",
    prefix: "ccolors",
    defaultColors:{
      fg: "#333",
      bg: "white"
  })
  .UseA11y()
  .Draw();
```

To use your own colors:

```js
Colors()
  .Init({
    selector: "#color-bar",
    prefix: "ccolors",
    // default colors can be ommitted to default to 
    // black foreground and white background
  })
  .ColorBar([
    { fg: 'yellow', bg: 'black' },
    ...
  ])
  .Draw();
```
